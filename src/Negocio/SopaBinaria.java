/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Bit;
import Util.LeerMatriz_Excel;
import Vista.GuiBinaria;
import java.io.IOException;
import static java.lang.Long.parseLong;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author madar
 */
public class SopaBinaria extends GuiBinaria {

    private Bit mySopaBinaria[][];
    private Bit arrayBinario[];
    
    public SopaBinaria() {
    }

    public SopaBinaria(String rutaArchivoExcel) throws IOException {
       
        LeerMatriz_Excel miExcel = new LeerMatriz_Excel(rutaArchivoExcel, 0);
    }

    //llenar mySopabinaria con datos tipo boolean
    public Bit[][] llenarSopa(String[][] nuevo) {
        mySopaBinaria = new Bit[nuevo.length][nuevo[0].length];

        for (int i = 0; i < mySopaBinaria.length; i++) {
            for (int j = 0; j < mySopaBinaria[i].length; j++) {
                String msg = nuevo[i][j];
                boolean c = msg.equals("1");

                Bit nuevoBit = new Bit(c);
                mySopaBinaria[i][j] = nuevoBit;
            }
        }
        return mySopaBinaria;
    }

    //Convierte un numero en binario
    public long convertirNumeroBinario(long num) {
        long aux = 0;
        String binario = "";
        long nuM = num;

        while (nuM > 0) {
            aux = (nuM % 2);
            binario = aux + binario;
            nuM = nuM / 2;
        }

        //retorna binario
        return parseLong(binario);
    }

    public String[] numeroSeparado(long numBin) {
        long nBinario = numBin;
        long auxBinario = 0;
        long aux = 0;
        int contDigitos = 0;
        while (nBinario > 0) {
            auxBinario = nBinario % 10;
            nBinario = nBinario / 10;
            contDigitos++;
        }
        String[] numSeparado = new String[contDigitos];

        for (int i = numSeparado.length - 1; i > -1; i--) {
            aux = numBin % 10;
            numSeparado[i] = Long.toString(aux);
            numBin = numBin / 10;
        }
        return numSeparado;
    }

    public Bit[] parseBit(String[] arrayNuevo) {
        arrayBinario = new Bit[arrayNuevo.length];

        for (int i = 0; i < arrayBinario.length; i++) {
            String msg = arrayNuevo[i];
            boolean c = msg.equals("1");

            Bit nuevoBit = new Bit(c);
            arrayBinario[i] = nuevoBit;
        }
        return arrayBinario;
    }
    //HASTA AQUI TODO BIEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    
    public int getCuantasVeces_Horizontal(Bit[][] matrizBin, Bit[] arrayBin) {
        int c= 0;

        for (int i = 0; i < matrizBin.length; i++) {
            for (int j = 0; j < matrizBin[i].length; j++) {
                int x= j;
                for (int k = 0; k < arrayBin.length;) {            

                    if (x < matrizBin[i].length && k < arrayBin.length) {
                        if (k == arrayBin.length-1 && matrizBin[i][x].isValor() == arrayBin[k].isValor()) {
                            c+=1;
                            k= arrayBin.length;
                        }
                        else if(matrizBin[i][x].isValor() == arrayBin[k].isValor()){
                                k++;
                                x++;
                        }
                        else k= arrayBin.length;
                    }
                    else k= arrayBin.length;
                }
            }
        }
        return c;
    }

    
    
    public int getCuantasVeces_Vertical(Bit[][] matrizBin, Bit[] arrayBin) {
        int c= 0;

        for (int i = 0; i < matrizBin.length; i++) {
            for (int j = matrizBin[i].length; j > 0; j--) {
                int x= j;
                for (int k = 0; k < arrayBin.length;) {

                    if (i < matrizBin[x].length && k < arrayBin.length) {
                        if (k == arrayBin.length-1 && matrizBin[x][i].isValor() == arrayBin[k].isValor()) {
                            c+=1;
                            k= arrayBin.length;
                        }
                        else if(matrizBin[x][i].isValor() == arrayBin[k].isValor()){
                                k++;
                                x++;
                        }
                        else k= arrayBin.length;
                    }
                    else k= arrayBin.length;
                }
            }
        }
        return c;
    }

    
    public int getCuantasVeces_VerticalVoltiao(Bit[][] matrizBin, Bit[] arrayBin) {
        int c= 0;

        for (int i = 0; i < matrizBin.length; i++) {
            for (int j = matrizBin[i].length; j > 0; j--) {
                int x= j;
                for (int k = 0; k < arrayBin.length;) {

                    if (i < matrizBin[x].length && k < arrayBin.length) {
                        if (k == arrayBin.length-1 && matrizBin[x][i].isValor() == arrayBin[k].isValor()) {
                            c+=1;
                            k= arrayBin.length;
                        }
                        else if(matrizBin[x][i].isValor() == arrayBin[k].isValor()){
                                k++;
                                x++;
                        }
                        else k= arrayBin.length;
                    }
                    else k= arrayBin.length;
                }
            }
        }
        return c;
    }
    
    
}
